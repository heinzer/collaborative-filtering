var express = require('express');
var request = require('request');

module.exports = {
  main: function (router) {
      router.get('/example/poster/:_id', function(req, res) {
          request('http://www.omdbapi.com/?t=' + String(req.params._id), function(error, response, body) {
              if (!error && response.statusCode == 200) {
                  json_body = JSON.parse(body);
                  if (!json_body.Error) {
                      if(json_body.Poster && json_body.Poster !== "N/A"){
                          request(json_body.Poster).pipe(res);
                      } else {
                          res.send('{"Error": "No poster"}')
                      }
                  } else {
                      res.send('{"Error": "No item could be found"}');
                  }
              } else {
                  res.send('{"Error": "Could not connect to IMDB"}');
              }
          });
      });

      router.get('/example/:_id', function(req, res) {
          request('http://www.omdbapi.com/?t=' + String(req.params._id), function(error, response, body) {
              if (!error && response.statusCode == 200) {
                  json_body = JSON.parse(body);
                  if (!json_body.Error) {
                      res.send(body);
                  } else {
                      res.send('{"Error": "No item could be found"}');
                  }
              } else {
                  res.send('{"Error": "Could not connect to IMDB"}');
              }
          });
      });
  }
};
