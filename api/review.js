var express = require('express');
var request = require('request');

module.exports = {
    main: function (router) {
        // get all reviews
        router.get('/review', function (req, res) {
            res.send('{"response": "ok", "reviews": ["review 1", "review 2", "review 3"]}');
        });

        // create review
        router.post('/review', function (req, res){
            res.send('{"response": "ok"}');
        });

        // edit review
        router.put('/review/:_id', function (req, res){
            res.send('{"response": "ok"}');
        });

        // delete review
        router.delete('/review/:_id', function (req, res){
            res.send('{"response": "ok"}');
        });
    }
};
