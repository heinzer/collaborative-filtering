var express = require('express');
var request = require('request');

module.exports = {
    main: function (router) {
        // get all items
        router.get('/item', function (req, res) {
            var items = req.db.get("items");
            items.find({}, function(err, results) {
                res.send(results)
            });
        });

        // get one item
        router.get('/item/:_id', function (req, res) {
            var items = req.db.get("items");
            items.find({_id: req.params._id}, function(err, results) {
                res.send(results)
            });
        });

        // create item
        router.post('/item', function (req, res){
            var items = req.db.get("items");
            var bodyJson = req.body;
            items.insert({ name: bodyJson.name, description: bodyJson.description, category: bodyJson.category, createdAt: new Date() });
            res.send('{"response": "ok"}');
        });

        // edit item
        router.put('/item/:_id', function (req, res){
            res.send('{"response": "ok"}');
        });

        // delete item
        router.delete('/item/:_id', function (req, res){
            var items = req.db.get("items");
            items.remove({ _id: req.params._id });
            res.send('{"response": "ok"}');
        });
    }
};
