var express = require('express');
var request = require('request');

module.exports = {
    main: function (router) {
        // get all recommendations
        router.get('/recommendation', function (req, res) {
            res.send('{"response": "ok", "recommendations": ["recommendation 1", "recommendation 2", "recommendation 3"]}');
        });

        // create recommendation
        router.post('/recommendation', function (req, res){
            res.send('{"response": "ok"}');
        });

        // edit recommendation
        router.put('/recommendation/:_id', function (req, res){
            res.send('{"response": "ok"}');
        });

        // delete item
        router.delete('/recommendation/:_id', function (req, res){
            res.send('{"response": "ok"}');
        });
    }
};
