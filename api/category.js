var express = require('express');
var request = require('request');

module.exports = {
    main: function (router) {
        // get all categories
        router.get('/category', function (req, res) {
            res.send('{"response": "ok", "categories": ["category 1", "category 2", "category 3"]}');
        });

        // create category
        router.post('/category', function (req, res){
            res.send('{"response": "ok"}');
        });

        // edit category
        router.put('/category/:_id', function (){
            res.send('{"response": "ok"}');
        });

        // delete
        router.delete('/category/:_id', function (req, res){
            res.send('{"response": "ok"}');
        });
    }
};
