var express = require('express');
var request = require('request');

module.exports = {
    main: function (router) {
        router.get('/user', function (req, res) {
            res.send('{"response": "ok", "users": {0: "xXxhax04x420noscopezxXx"}');
        });

        // create category
        router.post('/user', function (req, res){
            res.send('{"response": "ok"}');
        });

        router.get('/user/:_id', function (req, res) {
            res.send('{"response": "ok", "username": "xXxhax04x420noscopezxXx", "age": 16, "description": "edgy"}');
        });

        // edit category
        router.put('/user/:_id', function (req, res){
            res.send('{"response": "ok"}');
        });

        // delete
        router.delete('/user/:_id', function (req, res){
            res.send('{"response": "ok"}');
        });
    }
};
