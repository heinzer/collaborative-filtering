var express = require('express');
var request = require('request');
var bodyParser = require('body-parser')
var mongo = require('mongodb');
var monk = require('monk');

var db = monk('localhost:27017/collaborative-filtering')

var app = express();
app.use(bodyParser.json())
var router = express.Router();
require('./api/example.js').main(router);
require('./api/category.js').main(router);
require('./api/item.js').main(router);
require('./api/recommendation.js').main(router);
require('./api/review.js').main(router);
require('./api/user.js').main(router);

app.use(function(req,res,next){
    req.db = db;
    next();
});
app.use('/api', router);

process.stdin.resume();//so the program will not close instantly

function exitHandler(options, err) {
    db.close();
    if (options.cleanup) console.log('clean');
    if (err) console.log(err.stack);
    if (options.exit) process.exit();
}

//do something when app is closing
process.on('exit', exitHandler.bind(null,{cleanup:true}));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));

app.listen(13000);
